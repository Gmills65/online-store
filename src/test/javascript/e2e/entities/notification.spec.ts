import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Notification e2e test', () => {

    let navBarPage: NavBarPage;
    let notificationDialogPage: NotificationDialogPage;
    let notificationComponentsPage: NotificationComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Notifications', () => {
        navBarPage.goToEntity('notification');
        notificationComponentsPage = new NotificationComponentsPage();
        expect(notificationComponentsPage.getTitle())
            .toMatch(/storeApp.notification.home.title/);

    });

    it('should load create Notification dialog', () => {
        notificationComponentsPage.clickOnCreateButton();
        notificationDialogPage = new NotificationDialogPage();
        expect(notificationDialogPage.getModalTitle())
            .toMatch(/storeApp.notification.home.createOrEditLabel/);
        notificationDialogPage.close();
    });

    it('should create and save Notifications', () => {
        notificationComponentsPage.clickOnCreateButton();
        notificationDialogPage.setDateInput(12310020012301);
        expect(notificationDialogPage.getDateInput()).toMatch('2001-12-31T02:30');
        notificationDialogPage.setDetailsInput('details');
        expect(notificationDialogPage.getDetailsInput()).toMatch('details');
        notificationDialogPage.setSentDateInput(12310020012301);
        expect(notificationDialogPage.getSentDateInput()).toMatch('2001-12-31T02:30');
        notificationDialogPage.formatSelectLastOption();
        notificationDialogPage.setUserIdInput('5');
        expect(notificationDialogPage.getUserIdInput()).toMatch('5');
        notificationDialogPage.setProductIdInput('5');
        expect(notificationDialogPage.getProductIdInput()).toMatch('5');
        notificationDialogPage.save();
        expect(notificationDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class NotificationComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-notification div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class NotificationDialogPage {
    modalTitle = element(by.css('h4#myNotificationLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    dateInput = element(by.css('input#field_date'));
    detailsInput = element(by.css('input#field_details'));
    sentDateInput = element(by.css('input#field_sentDate'));
    formatSelect = element(by.css('select#field_format'));
    userIdInput = element(by.css('input#field_userId'));
    productIdInput = element(by.css('input#field_productId'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setDateInput = function(date) {
        this.dateInput.sendKeys(date);
    };

    getDateInput = function() {
        return this.dateInput.getAttribute('value');
    };

    setDetailsInput = function(details) {
        this.detailsInput.sendKeys(details);
    };

    getDetailsInput = function() {
        return this.detailsInput.getAttribute('value');
    };

    setSentDateInput = function(sentDate) {
        this.sentDateInput.sendKeys(sentDate);
    };

    getSentDateInput = function() {
        return this.sentDateInput.getAttribute('value');
    };

    setFormatSelect = function(format) {
        this.formatSelect.sendKeys(format);
    };

    getFormatSelect = function() {
        return this.formatSelect.element(by.css('option:checked')).getText();
    };

    formatSelectLastOption = function() {
        this.formatSelect.all(by.tagName('option')).last().click();
    };
    setUserIdInput = function(userId) {
        this.userIdInput.sendKeys(userId);
    };

    getUserIdInput = function() {
        return this.userIdInput.getAttribute('value');
    };

    setProductIdInput = function(productId) {
        this.productIdInput.sendKeys(productId);
    };

    getProductIdInput = function() {
        return this.productIdInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
